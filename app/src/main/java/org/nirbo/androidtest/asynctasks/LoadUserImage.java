package org.nirbo.androidtest.asynctasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class LoadUserImage extends AsyncTask<Void, Void, Bitmap> {
    String mImageUrl;
    ImageView mUserImageView;

    public LoadUserImage(String imageUrl, ImageView userImageView) {
        this.mImageUrl = imageUrl;
        this.mUserImageView = userImageView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap userImage = null;

        try {
            userImage = BitmapFactory.decodeStream(
                    (InputStream) new URL(mImageUrl).getContent()
            );

        } catch (IOException e) {
            e.printStackTrace();
        }

        return userImage;
    }

    @Override
    protected void onPostExecute(Bitmap userImage) {
        super.onPostExecute(userImage);

        if (userImage != null) {
            mUserImageView.setImageBitmap(userImage);
        }
    }
}
