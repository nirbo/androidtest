package org.nirbo.androidtest.listeners;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;

import org.nirbo.androidtest.fragments.AppAlertDialogFragment;

public class DialogFragmentOnClickListener implements View.OnClickListener {

    Activity mContext;
    FragmentManager fm;
    String mDialogTitle;
    String mDialogMessage;

    public DialogFragmentOnClickListener(Activity context, FragmentManager fm, @Nullable String title, @Nullable String message) {
        this.mContext = context;
        this.fm = fm;

        if (title != null) {
            this.mDialogTitle = title;
        }

        if (message != null) {
            this.mDialogMessage = message;
        }
    }

    @Override
    public void onClick(View v) {
        AppAlertDialogFragment alertDialogFragment = AppAlertDialogFragment.newInstance(mDialogTitle, mDialogMessage);
        alertDialogFragment.show(fm, "AlertDialogFragment");
    }
}
