package org.nirbo.androidtest.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nirbo.androidtest.R;
import org.nirbo.androidtest.asynctasks.LoadUserImage;
import org.nirbo.androidtest.listeners.DialogFragmentOnClickListener;
import org.nirbo.androidtest.model.ChatMessage;
import org.nirbo.androidtest.model.User;
import org.nirbo.androidtest.utilities.JSONFileParser;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

public class ChatFragment extends Fragment {

    private static final String MESSAGES_JSON = "messages.json";
    private static final String USERS_JSON = "users.json";
    private static final String NO_IMAGE_FLAG = "NONE";

    Activity mContext;
    LinearLayout mChatFragmentContainer;
    Map<String, ChatMessage> mMessagesMap;
    Map<String, User> mUsersMap;

    public ChatFragment() {
        super();
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_fragment_layout, parent, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = getActivity();

        getMessagesJson();
        getUsersJson();

        if (mMessagesMap != null && mUsersMap != null) {
            populateChatMessages();
            populateContactGrid();
            attachOnClickListeners();
        }
    }

    // This method populates the user chat messages according to the data in the JSON files
    private void populateChatMessages() {
        mChatFragmentContainer = (LinearLayout) mContext.findViewById(R.id.chat_messages_container);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (Map.Entry<String, ChatMessage> messageEntry : mMessagesMap.entrySet()) {
            String key = messageEntry.getKey();
            ChatMessage value = messageEntry.getValue();

            View view = null;
            if (mUsersMap.containsKey(key)) {
                view = inflater.inflate(R.layout.message_element_layout, null);
                TextView senderName = (TextView) view.findViewById(R.id.user_name);
                TextView senderMessage = (TextView) view.findViewById(R.id.user_message);
                ImageView senderImage = (ImageView) view.findViewById(R.id.user_image);
                TextView messageTime = (TextView) view.findViewById(R.id.message_time);

                String userName = mUsersMap.get(key).getName();
                String userImageUrl = mUsersMap.get(key).getImageUrl();

                if (NO_IMAGE_FLAG.equals(userImageUrl)) {
                    senderImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.user_image_missing_background));
                } else {
                    fetchUserImageFromUrl(userImageUrl, senderImage);
                }
                senderName.setText(userName);
                senderMessage.setText(value.getContent());
                messageTime.setText(getCurrentHourMinute());

                senderMessage.setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(),
                        userName + "'s message clicked", value.getContent()));
                senderImage.setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(),
                        userName + "'s photo clicked", null));
            }

            mChatFragmentContainer.addView(view);
        }

        mChatFragmentContainer.invalidate();
    }

    // Populates the contacts grid in the ChatFragment with images and names of the users
    private void populateContactGrid() {
        ImageView gridImageView1 = (ImageView) mContext.findViewById(R.id.grid_image_1);
        ImageView gridImageView2 = (ImageView) mContext.findViewById(R.id.grid_image_2);
        ImageView gridImageView3 = (ImageView) mContext.findViewById(R.id.grid_image_3);
        ImageView gridImageView4 = (ImageView) mContext.findViewById(R.id.grid_image_4);
        ImageView gridImageView5 = (ImageView) mContext.findViewById(R.id.grid_image_5);
        ImageView gridImageView6 = (ImageView) mContext.findViewById(R.id.grid_image_6);

        List<ImageView> gridImageViewsList = new ArrayList<ImageView>();
        gridImageViewsList.add(gridImageView1);
        gridImageViewsList.add(gridImageView2);
        gridImageViewsList.add(gridImageView3);
        gridImageViewsList.add(gridImageView4);
        gridImageViewsList.add(gridImageView5);
        gridImageViewsList.add(gridImageView6);

        TextView gridTextView1 = (TextView) mContext.findViewById(R.id.grid_image_1_name);
        TextView gridTextView2 = (TextView) mContext.findViewById(R.id.grid_image_2_name);
        TextView gridTextView3 = (TextView) mContext.findViewById(R.id.grid_image_3_name);
        TextView gridTextView4 = (TextView) mContext.findViewById(R.id.grid_image_4_name);
        TextView gridTextView5 = (TextView) mContext.findViewById(R.id.grid_image_5_name);
        TextView gridTextView6 = (TextView) mContext.findViewById(R.id.grid_image_6_name);

        List<TextView> gridTextViewsList = new ArrayList<TextView>();
        gridTextViewsList.add(gridTextView1);
        gridTextViewsList.add(gridTextView2);
        gridTextViewsList.add(gridTextView3);
        gridTextViewsList.add(gridTextView4);
        gridTextViewsList.add(gridTextView5);
        gridTextViewsList.add(gridTextView6);

        for (int index = 0; index < gridImageViewsList.size(); index++) {
            Object[] userMapKeysArray = mUsersMap.keySet().toArray();
            String userMapKey = (String) userMapKeysArray[index];
            User userEntry = mUsersMap.get(userMapKey);

            fetchUserImageFromUrl(userEntry.getImageUrl(), gridImageViewsList.get(index));
            gridTextViewsList.get(index).setText(userEntry.getName());

            gridImageViewsList.get(index).setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(),
                    gridTextViewsList.get(index).getText().toString() + "'s photo clicked", null));
        }
    }

    // Attach AlertDialog OnClickListeners to various views and buttons
    private void attachOnClickListeners() {
        Button composeButton = (Button) mContext.findViewById(R.id.compose_button);
        composeButton.setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(), "Compose Button Clicked!", "Great Success!"));
        ImageView toolbarButton1 = (ImageView) mContext.findViewById(R.id.toolbar_button_1);
        toolbarButton1.setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(), "ToolBar Button 1 Clicked!", null));
        ImageView toolbarButton2 = (ImageView) mContext.findViewById(R.id.toolbar_button_2);
        toolbarButton2.setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(), "ToolBar Button 2 Clicked!", null));
        ImageView toolbarButton3 = (ImageView) mContext.findViewById(R.id.toolbar_button_3);
        toolbarButton3.setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(), "ToolBar Button 3 Clicked!", null));
        ImageView toolbarButton4 = (ImageView) mContext.findViewById(R.id.toolbar_button_4);
        toolbarButton4.setOnClickListener(new DialogFragmentOnClickListener(mContext, getFragmentManager(), "ToolBar Button 4 Clicked!", null));
    }

    // Parse the messages JSON file and store the JSON objects in a TreeMap
    private void getMessagesJson() {
        String messagesJson = parseJsonAsset(MESSAGES_JSON);
        mMessagesMap = new TreeMap<String, ChatMessage>(String.CASE_INSENSITIVE_ORDER);

        try {
            JSONArray jsonArray = new JSONArray(messagesJson);

            for (int index = 0; index < jsonArray.length(); index++) {
                ChatMessage message = new ChatMessage();
                JSONObject currentJsonObject = jsonArray.getJSONObject(index);

                String messageSender = currentJsonObject.getString("From");
                String messageContent = currentJsonObject.getString("Message");

                message.setSender(messageSender);
                message.setContent(messageContent);

                mMessagesMap.put(messageSender, message);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Parse the users JSON file and store the JSON objects in a TreeMap
    private void getUsersJson() {
        String usersJson = parseJsonAsset(USERS_JSON);
        mUsersMap = new TreeMap<String, User>(String.CASE_INSENSITIVE_ORDER);

        try {
            JSONArray jsonArray = new JSONArray(usersJson);

            for (int i = 0; i < jsonArray.length(); i++) {
                User user = new User();
                JSONObject currentJsonObject = jsonArray.getJSONObject(i);

                String userName = currentJsonObject.getString("name");
                String userId = currentJsonObject.getString("id");
                String userImageUrl = currentJsonObject.getString("img");

                user.setName(userName);
                user.setId(userId);
                if ("".equals(userImageUrl) || userImageUrl == null) {
                    user.setImageUrl(NO_IMAGE_FLAG);
                } else {
                    user.setImageUrl(userImageUrl);
                }

                mUsersMap.put(userId, user);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* Re-usable method that fetches the JSON content from the asset filename passed to it
       This method is used in both getUsersJson() and getMessagesJson() */
    private String parseJsonAsset(String assetName) {
        String json = null;
        JSONFileParser parser = new JSONFileParser();

        try {
            InputStream jsonAsset = mContext.getAssets().open(assetName);
            json = parser.readJSONFromAsset(jsonAsset);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    // Launches an AsyncTask that fetches the users' photos from the provided URL and displays them in the provided ImageView
    private void fetchUserImageFromUrl(String imageUrl, ImageView imageView) {
        new LoadUserImage(imageUrl, imageView)
                .execute();
    }

    // Returns the (formatted) current hour and minute as per the timezone configured on the device
    private String getCurrentHourMinute() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date currentTime = calendar.getTime();
        DateFormat timeFormat = new SimpleDateFormat("H:mm");

        return timeFormat.format(currentTime);
    }
}
