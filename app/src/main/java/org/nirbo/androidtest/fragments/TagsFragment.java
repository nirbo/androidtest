package org.nirbo.androidtest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.nirbo.androidtest.R;

public class TagsFragment extends Fragment {

    public TagsFragment() {
        super();
    }

    public static TagsFragment newInstance() {
        return new TagsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tags_fragment_layout, parent, false);
    }
}
