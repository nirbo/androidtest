package org.nirbo.androidtest.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class AppAlertDialogFragment extends DialogFragment {

    String mTitle;
    String mMessage;

    public AppAlertDialogFragment() {}

    public AppAlertDialogFragment(String title, String message) {
        this.mTitle = title;
        this.mMessage = message;
    }

    public static AppAlertDialogFragment newInstance(String title, String message) {
        return new AppAlertDialogFragment(title, message);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_input_add)
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
    }
}
