package org.nirbo.androidtest.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import com.astuetz.PagerSlidingTabStrip;

import org.nirbo.androidtest.R;
import org.nirbo.androidtest.adapters.TabsViewPagerAdapter;

public class MainActivity extends ActionBarActivity {

    private PagerSlidingTabStrip mSlidingTabs;
    private ViewPager mTabsViewPager;
    private Toolbar mToolbar;
    private int mTabsCount = 3;
    private int[] mTabsIcons = {
            R.drawable.chat_tab_icon,
            R.drawable.contacts_tab_icon,
            R.drawable.tags_tab_icon
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);

        initViewPager();
        initSlidingTabs();
        initToolbar();
        setStatusBarColor();
    }

    // Sets up the ViewPager
    private void initViewPager() {
        mTabsViewPager = (ViewPager) findViewById(R.id.tabs_viewpager);
        mTabsViewPager.setOffscreenPageLimit(2);
        TabsViewPagerAdapter viewPagerAdapter = new TabsViewPagerAdapter(getSupportFragmentManager(), mTabsCount, mTabsIcons);
        mTabsViewPager.setAdapter(viewPagerAdapter);

    }

    // Sets up the Sliding Tabs bar at the top
    private void initSlidingTabs() {
        mSlidingTabs = (PagerSlidingTabStrip) findViewById(R.id.sliding_tabs);
        mSlidingTabs.setViewPager(mTabsViewPager);
    }

    // Sets up the Toolbar at the bottom
    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.bottom_bar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setContentInsetsAbsolute(0, mToolbar.getWidth());
    }

    // Ensures the Status Bar is black in Lollipop
    private void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.Black));
        }
    }
}
