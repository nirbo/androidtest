package org.nirbo.androidtest.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.astuetz.PagerSlidingTabStrip;

import org.nirbo.androidtest.fragments.ChatFragment;
import org.nirbo.androidtest.fragments.ContactsFragment;
import org.nirbo.androidtest.fragments.TagsFragment;

public class TabsViewPagerAdapter extends FragmentStatePagerAdapter
        implements PagerSlidingTabStrip.IconTabProvider {

    private int mTabsCount;
    private int[] mTabsIcons;

    public TabsViewPagerAdapter(FragmentManager fm, int tabsCount, int[] tabsIcons) {
        super(fm);
        this.mTabsCount = tabsCount;
        this.mTabsIcons = tabsIcons;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ChatFragment.newInstance();
            case 1:
                return ContactsFragment.newInstance();
            case 2:
                return TagsFragment.newInstance();
        }

        return null;
    }

    @Override
    public int getCount() {
        return mTabsCount;
    }

    @Override
    public int getPageIconResId(int position) {
        return mTabsIcons[position];
    }
}
