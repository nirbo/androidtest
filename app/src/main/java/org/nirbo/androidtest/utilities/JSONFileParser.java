package org.nirbo.androidtest.utilities;

import java.io.IOException;
import java.io.InputStream;

public class JSONFileParser {

    public String readJSONFromAsset(InputStream jsonAsset) {
        String json;

        try {
            int size = jsonAsset.available();
            byte[] buffer = new byte[size];
            jsonAsset.read(buffer);
            jsonAsset.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        }

        return json;
    }

}
